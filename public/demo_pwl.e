* demo du pre-processeur aime_pwl
* utilisation : aime_pwl.pl demo_pwl.e
* cette commande va creer un fichier demo_pwl.e2 dans lequel les sources PWL
* sont completees en fonction des sequences de bits donnees dans chaque ligne *pwlbin
* c'est le fichier .e2 qu'on devra simuler

* Enable HSPICE post-processing
.options post nomod 

rA aa 0 100
rB bb 0 100

vA aa 0 PWL
*pwlbin 0.5 100 010010
vB bb 0 PWL
*pwlbin 0.5 100 111000

* ici chaque bit va durer 100ns, il y a 6 bits, donc Tstop = 600ns 
.TRAN 1n 600n

.end
