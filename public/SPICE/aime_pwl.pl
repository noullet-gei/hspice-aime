use warnings;
use strict;

sub usage {
printf("aime_pwl : generateur de sources PWL pour SPICE\n");
printf("           application : signaux binaires 5 Volts\n");
printf("usage    : perl aime_pwl.pl <nom de fichier source HSPICE>\n");
printf("format   : dans le fichier source,\n");   
printf("           *pwlbin [tr] [tbit] [bits]\n");
printf("           avec : tr = temps de montee et descente (ns)\n");
printf("                  tbit = duree d'un bit (ns)\n");
printf("                  bits = chaine de caracteres '0' et '1'\n");
printf("exemple  : Vsignal entree gnd PWL\n" );
printf("           *pwlbin 2 40 0011011100101000011\n" );
exit();
}

# niveaux de tension
my $vol=0.0; my $voh=5.0;
# limite ligne de texte
my $maxlen = 72;

# generer les parametres de la source pwl a partir de
# la description binaire au format *pwlbin
sub pwlgen {
my @fields = split( '\s+', $_[0] );
die "erreur : nombre de champs apres *pwlbin\n" if ($#fields != 3);
my $tr = $fields[1];
die "erreur tr\n" if ( $tr <= 0.0 );
my $tbit = $fields[2];
die "erreur duree bit\n" if ( $tbit <= $tr );
my $nbits = length( $fields[3] );
die "erreur liste bit\n" if ( $fields[3] !~ /^[01]+$/ );
my $oldbit = substr( $fields[3], 0, 1 );
my $newbit = $oldbit;
my $t = 0.0;
my $res = sprintf( "+ %gN %g ", $t, ($oldbit=='1')?$voh:$vol );
$t += $tbit;
my $point;
my $curlen = length( $res );
for ( my $pos = 1; $pos < $nbits; $pos++ ) {
    $newbit = substr( $fields[3], $pos, 1 );
    if ( $newbit != $oldbit ) {
       $point = sprintf( "%gN %g %gN %g ", $t, ($oldbit=='1')?$voh:$vol,
	                               $t+$tr, ($newbit=='1')?$voh:$vol );
       $curlen += length( $point );
       if ( $curlen >= $maxlen ) {
	  $res .= "\n+ "; $curlen = 2 + length( $point );
          }
       $res .= $point;
       $oldbit = $newbit;
       }
    $t += $tbit;
    }
$point = sprintf( "%gN %g\n", $t, ($newbit=='1')?$voh:$vol );
$curlen += length( $point );
if ( $curlen >= $maxlen ) {
   $res .= "\n+ ";
   }
$res .= $point;
return $res;
}

my $fnam = $ARGV[0];
$fnam or usage();
open INFILE, "< $fnam" or die "erreur ouverture fichier $fnam\n";
$fnam .= "2";
open OUTFILE, "> $fnam" or die "erreur ouverture fichier $fnam\n";

while( my $ligne = <INFILE> )  {
   if ( $ligne =~ /^[*]pwlbin/ )  {
      print OUTFILE pwlgen( $ligne );
      }
   print OUTFILE $ligne;
   }

close INFILE; close OUTFILE;
