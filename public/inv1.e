* caracteristique statique de 2 inverseurs CMOS en cascade

* modeles des transistors ---------------

.inc ./cmos47.mod' 

* definitions de sous-circuits ----------

.subckt inv1 v e s
Mn s e 0 0 modn w=2u l=0.6u
Mp s e v v modp w=2u l=0.6u
.ends

* circuit principal ---------------------
* ici 2 sources et 2 inverseurs

vcc vdd 0 DC 5
vin vin 0 DC 1
X1 vdd vin v1 inv1
X2 vdd v1  v2 inv1

* directive d'analyse -------------------

.DC vin 0 5 0.001

.end
