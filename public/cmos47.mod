* 5 Volts, 0.6 um CMOS process models

* Default diffusions:
.OPTIONS DEFAD=2.56e-12 DEFAS=2.56e-12
.OPTIONS DEFPD=6.4e-06 DEFPS=6.4e-06 
.OPTIONS DEFNRD=0.1 DEFNRS=0.1 
.options defl=0.6u

* Model parameters
.OPTIONS TNOM=27.000 DCAP=1
*
* Enable HSPICE post-processing
.options post nomod 
*
* Set default tolerances:
.OPTIONS RELTOL=1.0e-3 VNTOL=1.0e-6 ABSTOL=1.0e-9 PIVOT=10
*
* Set Gear integration with LTE time-step control:
.OPTIONS METHOD=GEAR MAXORD=2 LVLTIM=2 TRTOL=7.0 
.OPTIONS CHGTOL=1e-14 RELQ=1e-3 
* ----------------------------------------------------------------------
*                        TYPICAL MEAN CONDITION
* ----------------------------------------------------------------------
.MODEL MODN NMOS LEVEL=47 
*
*        *** Flags ***
+SUBTHMOD=3.000e+00 SATMOD =2.000e+00 BULKMOD=1.000e+00 
*        *** Threshold voltage related model parameters ***
+K1     =9.270e-01 
+K2     =-8.02e-02 K3     =4.864e+01 K3B    =-4.04e-01 
+NPEAK  =9.515e+16 VTH0   =7.968e-01 
+VOFF   =-7.16e-02 DVT0   =1.090e+00 DVT1   =7.910e-01 
+DVT2   =-3.93e-01 KETA   =-2.79e-02 
+PSCBE1 =0.000e+00 PSCBE2 =1.000e-10 
*        *** Mobility related model parameters ***
+UA     =4.400e-10 UB     =7.405e-19 UC     =-1.27e-02 
+U0     =4.426e+02 
*        *** Subthreshold related parameters ***
+DSUB   =5.000e-01 ETA0   =3.664e-02 ETAB   =-2.44e-03 
+NFACTOR=7.659e-01 VGHIGH =1.500e-01 VGLOW  =-1.10e-01 
*        *** Saturation related parameters ***
+ETA    =3.000e-01 PCLM   =1.433e+00 
+PDIBL1 =4.655e-02 PDIBL2 =7.138e-03 DROUT  =5.000e-01 
+A0     =5.813e-01 A1     =0.000e+00 A2     =1.000e+00 
+PVAG   =0.000e+00 VSAT   =8.979e+06 
*        *** Geometry modulation related parameters ***
+LDD    =2.000e-07 W0     =2.611e-06 
*        *** Temperature effect parameters ***
+AT     =3.300e+04 UTE    =-1.78e+00 
+KT1    =-3.72e-01 KT2    =2.200e-02 KT1L   =0.000e+00 
+UA1    =0.000e+00 UB1    =0.000e+00 UC1    =0.000e+00 
*        *** Overlap capacitance related and dynamic model parameters   ***
+CGDO   =3.400e-10 CGSO   =3.400e-10 CGBO   =1.300e-10 
+XPART  =1.000e+00 
*        *** Parasitic resistance and capacitance related model parameters ***
+RDS0   =1.000e+00 RDSW   =1.703e+03 
+CDSC   =1.058e-03 CDSCB  =0.000e+00 CIT    =0.000e+00 
*        *** Process and parameters extraction related model parameters ***
+TOX    =1.264e-08 
+NLX    =1.000e-10 
+XL     =-5.00e-08 XW     =0.000e+00 
*        *** Noise effect related model parameters ***
+AF     =1.451e+00 KF     =2.340e-26 
+NLEV   =0 
*        *** Common extrinsic model parameters ***
+ACM    =2        
+RD     =0.000e+00 RS     =0.000e+00 RSH    =3.000e+01 
+RDC    =0.000e+00 RSC    =0.000e+00 
+LD     =1.545e-07 WD     =-3.20e-09 
+LDIF   =0.000e+00 HDIF   =8.000e-07 WMLT   =1.000e+00 
+LMLT   =1.000e+00 XJ     =3.000e-07 
+JS     =1.000e-05 JSW    =0.000e+00 IS     =0.000e+00 
+N      =1.000e+00 NDS    =1000. VNDS   =-1.000e+00 
+CBD    =0.000e+00 CBS    =0.000e+00 CJ     =3.500e-04 
+CJSW   =3.100e-10 FC     =0.000e+00 
+MJ     =4.500e-01 MJSW   =4.900e-01 TT     =0.000e+00 
+PB     =9.400e-01 PHP    =9.400e-01 
* ----------------------------------------------------------------------
.MODEL MODP PMOS LEVEL=47 
*
*        *** Flags ***
+SUBTHMOD=3.000e+00 SATMOD =2.000e+00 BULKMOD=1.000e+00 
*        *** Threshold voltage related model parameters ***
+K1     =4.040e-01 
+K2     =2.398e-02 K3     =4.760e+00 K3B    =-1.93e-01 
+NPEAK  =3.681e+16 VTH0   =-9.49e-01 
+VOFF   =-7.53e-02 DVT0   =1.929e+00 DVT1   =1.087e+00 
+DVT2   =-4.51e-02 KETA   =1.527e-02 
+PSCBE1 =4.700e+08 PSCBE2 =1.000e-05 
*        *** Mobility related model parameters ***
+UA     =0.000e+00 UB     =8.473e-19 UC     =-3.50e-02 
+U0     =1.132e+02 
*        *** Subthreshold related parameters ***
+DSUB   =5.000e-01 ETA0   =6.314e-02 ETAB   =0.000e+00 
+NFACTOR=8.483e-01 VGHIGH =1.500e-01 VGLOW  =-1.10e-01 
*        *** Saturation related parameters ***
+ETA    =3.000e-01 PCLM   =3.019e+00 
+PDIBL1 =3.885e-01 PDIBL2 =0.000e+00 DROUT  =5.000e-01 
+A0     =5.953e-01 A1     =0.000e+00 A2     =1.000e+00 
+PVAG   =1.537e+00 VSAT   =7.490e+06 
*        *** Geometry modulation related parameters ***
+LDD    =2.000e-07 W0     =0.000e+00 
*        *** Temperature effect parameters ***
+AT     =3.300e+04 UTE    =-1.42e+00 
+KT1    =-4.89e-01 KT2    =2.200e-02 KT1L   =0.000e+00 
+UA1    =0.000e+00 UB1    =0.000e+00 UC1    =0.000e+00 
*        *** Overlap capacitance related and dynamic model parameters   ***
+CGDO   =3.400e-10 CGSO   =3.400e-10 CGBO   =1.300e-10 
+XPART  =1.000e+00 
*        *** Parasitic resistance and capacitance related model parameters ***
+RDS0   =1.000e+00 RDSW   =4.516e+03 
+CDSC   =8.221e-04 CDSCB  =0.000e+00 CIT    =9.958e-05 
*        *** Process and parameters extraction related model parameters ***
+TOX    =1.264e-08 
+NLX    =2.230e-07 
+XL     =-5.00e-08 XW     =0.000e+00 
*        *** Noise effect related model parameters ***
+AF     =1.279e+00 KF     =6.314e-29 
+NLEV   =0 
*        *** Common extrinsic model parameters ***
+ACM    =2        
+RD     =0.000e+00 RS     =0.000e+00 RSH    =5.700e+01 
+RDC    =0.000e+00 RSC    =0.000e+00 
+LD     =7.134e-08 WD     =3.122e-08 
+LDIF   =0.000e+00 HDIF   =8.000e-07 WMLT   =1.000e+00 
+LMLT   =1.000e+00 XJ     =3.000e-07 
+JS     =4.000e-05 JSW    =0.000e+00 IS     =0.000e+00 
+N      =1.000e+00 NDS    =1000. VNDS   =-1.000e+00 
+CBD    =0.000e+00 CBS    =0.000e+00 CJ     =4.400e-04 
+CJSW   =3.100e-10 FC     =0.000e+00 
+MJ     =5.600e-01 MJSW   =3.300e-01 TT     =0.000e+00 
+PB     =9.100e-01 PHP    =9.100e-01 
* ----------------------------------------------------------------------
